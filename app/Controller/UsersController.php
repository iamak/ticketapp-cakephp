<?php

/**
* Users Controller
*/

App::uses('AppController', 'Controller');

class UsersController extends AppController
{
	var $uses = array('User', 'Group', 'UserGroup');

	public $paginate = array(        
        'User' => array(
        	'conditions' => ['User.active' => 1],
        	'limit' => 25,
	        'order' => array(
	            'User.id' => 'desc'
	        )
        ),
    );

	public function beforeFilter() {
	    parent::beforeFilter();
	    // Allow users to register and logout.
	    $this->Auth->allow('register', 'login', 'logout');

	    if ($this->action != 'login' && $this->action != 'register' && $this->action != 'logout')
        {
            if(!AuthComponent::user('admin')){
				$this->redirect('/');
			}
        }
	}	
	
	public function index()
	{
		$this->set('title','Users :: Ticket App');
		$this->set('page_title','Users');

		$this->Paginator->settings = $this->paginate;

		$Users = $this->Paginator->paginate('User');

		$this->set('Users', $Users);
	}
	
	/**
	 * User Login	 
	 */
	public function login()
	{
		//if already logged-in, redirect to dashboard
        if($this->Session->check('Auth.User')){

            $this->redirect('/');

        }

		$this->layout = 'auth';
		$this->set('title','Login');
		$this->set('page_title','Login');

		// if we get the post information, try to authenticate
        if ($this->request->is('post')) {

	        if ($this->Auth->login()) {
	            return $this->redirect($this->Auth->redirectUrl());
	        }

	        $this->Session->setFlash(__('Username or password is incorrect'));
	    } 
	}

	/**
	 * Logout Function
	 */
	public function logout() 
	{
		return $this->redirect($this->Auth->logout());

    }

    /**
     * Register User in the Front End
     */
	public function register()
	{
		//if already logged-in, redirect
        if($this->Session->check('Auth.User')){

            $this->redirect('/');

        }

        if ($this->request->is('post')) {

        	// user group for customers
            $this->request->data['User']['user_group_id'] = 1;
            $this->request->data['User']['status'] = 'New User';

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('The user has been created'));

                $this->redirect(array('action' => 'login'));

            } else {

                $this->Session->setFlash(__('The user could not be created. Please, try again.'));

            }   
        }

		$this->layout = 'auth';
		$this->set('title','Register');
		$this->set('page_title','Register');
	}

	public function add()
	{		
		$this->set('title','Add App User :: Ticket App');
		$this->set('page_title','Add New App User');

		$Groups = $this->Group->find('list', [
					'conditions' => ['Group.active' => 1],
					'order' => ['Group.name ASC'],
					'fields'=> ['id','name'],
				]);
		$this->set('Groups', $Groups);

        if ($this->request->is('post')) {
        	//var_dump($this->request->data);
        	//exit;
	        // admin id
            $this->request->data['User']['created_by'] = AuthComponent::user('id');
            $this->request->data['User']['updated_by'] = AuthComponent::user('id');
            $this->request->data['User']['user_group_id'] = 2;
            $this->request->data['User']['app_user'] = 1;
            $this->request->data['User']['status'] = 'New App User';

            $this->User->create();

            if ($this->User->save($this->request->data)) {
                $this->Flash->set('The user has been created', array(
                	'params' => array(
                		'class' => 'alert alert-success',
                	),
                ));
                $this->redirect(array('action' => 'add'));
            } else {
                $this->Flash->set('The user could not be created. Please, try again.', array(
                	'params' => array(
                		'class' => 'alert alert-danger',
                	),
                ));
            }
	    } 
	}

	public function edit($id = null)
	{
		$this->set('title','Edit User :: Ticket App');
		$this->set('page_title','Edit User');

		$Groups = $this->Group->find('list', [
					'conditions' => ['Group.active' => 1],
					'order' => ['Group.name ASC'],
					'fields'=> ['id','name'],
				]);
		$this->set('Groups', $Groups);		

		$User = $this->User->findById($id);

		$this->set('User',$User['User']);
		
		if(empty($this->request->data)){
			$this->request->data = $User;
		}
        else{
        	if($this->request->is('put')) {
		        // admin id
	            $this->request->data['User']['updated_by'] = AuthComponent::user('id');

	            if ($this->User->save($this->request->data)) {
	                $this->Flash->set('The User has been saved', array(
	                	'params' => array(
	                		'class' => 'alert alert-success',
	                	),
	                ));
	            } else {
	                $this->Flash->set('The User could not be saved. Please, try again.', array(
	                	'params' => array(
	                		'class' => 'alert alert-danger',
	                	),
	                ));
	            }
		    }
        }
	}

	public function delete($id = null){
		if (!$id) {  

			$this->Flash->set('Invalid user id', array(
	                	'params' => array(
	                		'class' => 'alert alert-danger',
	                	),
	                )); 

			$this->redirect(array('action' => 'index'));  

		}
		if($this->request->is('post')) {
			$User = $this->User->findById($id);		
			$this->request->data = $User;
			$this->request->data['User']['id'] = $User['User']['id'];
			$this->request->data['User']['active'] = 0;
			$this->request->data['User']['status'] = 'Deleted';
			$this->request->data['User']['updated_by'] = AuthComponent::user('id');
			
			$this->User->save($this->request->data['User']);
			$this->redirect($this->referer());
		}
		else{
			$this->redirect($this->referer());
		}
	}
}