<?php

/**
* Dashboard Controller
*/

App::uses('AppController', 'Controller');

class GroupsController extends AppController
{
	var $uses = array('User', 'Group');

	public $paginate = array(        
        'Group' => array(
        	'conditions' => ['Group.active' => 1],
        	'limit' => 25,
	        'order' => array(
	            'Group.id' => 'desc'
	        )
        ),
    );    

	public function beforeFilter()
	{
		parent::beforeFilter();
		if(!AuthComponent::user('admin')){
			$this->redirect('/');
		}
	}
	/**
	 * list all user groups
	 * @return view
	 */
	public function index()
	{
		$this->set('title','Manage Groups :: Ticket App');
		$this->set('page_title','Manage Groups');

		$this->Paginator->settings = $this->paginate;

		$Groups = $this->Paginator->paginate('Group');

		$this->set('Groups', $Groups);
	}


	public function add()
	{		
		$this->set('title','Add Group :: Ticket App');
		$this->set('page_title','Add New Group');

		$Users = $this->User->find('list', [
					'conditions' => ['User.active' => 1, 'User.app_user' => 1],
					'order' => ['User.first_name ASC'],
					'fields'=> ['id','username'],
				]);
		$this->set('Users', $Users);

		// if we get the post information, try to authenticate
        if ($this->request->is('post')) {
	        // admin id
            $this->request->data['Group']['created_by'] = AuthComponent::user('id');
            $this->request->data['Group']['updated_by'] = AuthComponent::user('id');

            $this->Group->create();

            if ($this->Group->save($this->request->data)) {
                $this->Flash->set('The group has been created', array(
                	'params' => array(
                		'class' => 'alert alert-success',
                	),
                ));
                $this->redirect(array('action' => 'add'));
            } else {
                $this->Flash->set('The group could not be created. Please, try again.', array(
                	'params' => array(
                		'class' => 'alert alert-danger',
                	),
                ));
            }
	    } 
	}

	public function edit($id = null)
	{
		$this->set('title','Edit Group :: Ticket App');
		$this->set('page_title','Edit Group');

		if (!$id) {  

			$this->Flash->set('Invalid group id', array(
	                	'params' => array(
	                		'class' => 'alert alert-danger',
	                	),
	                )); 

			$this->redirect(array('action' => 'index'));  

		}  

		$Users = $this->User->find('list', [
					'conditions' => ['User.active' => 1, 'User.app_user' => 1],
					'order' => ['User.first_name ASC'],
					'fields'=> ['id','username'],
				]);
		$this->set('Users', $Users);		

		$Group = $this->Group->findById($id);

		$this->set('Group',$Group['Group']);
		
		if(empty($this->request->data)){
			$this->request->data = $Group;
		}
        else{
        	if($this->request->is('put')) {
		        // admin id
	            $this->request->data['Group']['updated_by'] = AuthComponent::user('id');

	            if ($this->Group->save($this->request->data)) {
	                $this->Flash->set('The group has been saved', array(
	                	'params' => array(
	                		'class' => 'alert alert-success',
	                	),
	                ));
	                //$this->redirect(array('action' => 'edit'));
	            } else {
	                $this->Flash->set('The group could not be saved. Please, try again.', array(
	                	'params' => array(
	                		'class' => 'alert alert-danger',
	                	),
	                ));
	            }
		    }
        }
	}

	public function delete($id = null){
		if (!$id) {  

			$this->Flash->set('Invalid group id', array(
	                	'params' => array(
	                		'class' => 'alert alert-danger',
	                	),
	                )); 

			$this->redirect(array('action' => 'index'));  

		}
		if($this->request->is('post')) {
			$Group = $this->Group->findById($id);
			// soft deleting
			$this->request->data = $Group['Group'];
			$this->request->data['active'] = 0;
			$this->Group->save($this->request->data);
			$this->redirect($this->referer());
		}
		else{
			$this->redirect($this->referer());
		}
	}
}