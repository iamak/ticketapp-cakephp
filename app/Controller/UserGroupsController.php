<?php

/**
* Dashboard Controller
*/

App::uses('AppController', 'Controller');

class UserGroupsController extends AppController
{
	/**
	 * list all user groups
	 * @return view
	 */
	public function index()
	{
		$this->set('title','Manage User Groups :: Ticket App');
		$this->set('page_title','Manage User Groups');
	}


	public function add()
	{
		$this->set('title','Add User Group :: Ticket App');
		$this->set('page_title','Add New User Group');
	}
}