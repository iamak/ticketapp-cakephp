<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {
	
	var $uses = array('User', 'Group', 'Message', 'MessageAction');

	public $components = array(
				'DebugKit.Toolbar',
		        'Session',
		        'Flash',
		        'Paginator',
		        'Auth' => array(
		        	'authenticate' => array(
		        		'Form' => array(
		        			'userModel' => 'User',
		        			'scope' => array('User.active' => 1),
		        			'passwordHasher' => 'Blowfish',
		        		),		        		
		        	),
		        ),
			);

	public function beforeFilter()
	{
		// change default layout to use new template
		$this->layout = 'master';
	}

	public $Priorities = ['Normal' => 'Normal', 'Low' =>  'Low', 'Urgent' => 'Urgent'];

	public $Ranks = ['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5];
}
