<?php

/**
* Dashboard Controller
*/

App::uses('AppController', 'Controller');

class DashboardsController extends AppController
{
	var $uses = array('User', 'Group', 'Message');	

    public $paginate = array(
        'Message' => array(
        	'conditions' => ['Message.active' => 1],
        	'limit' => 25,
	        'order' => array(
	            'Message.id' => 'desc'
	        )
        ),
        'User' => array(
        	'conditions' => ['User.active' => 1],
        	'limit' => 25,
	        'order' => array(
	            'User.id' => 'desc'
	        )
        ),
    );
	
	public function index()
	{
		$this->set('title','Dashboard :: Ticket App');
		$this->set('page_title','Dashboard');

		$Groups = $this->Group->find('list', [
					'conditions' => ['Group.active' => 1],
					'order' => ['Group.id DESC'],
					'fields'=> ['id','name'],
				]);

		$this->set('Groups', $Groups);

		$this->Paginator->settings = $this->paginate;

		$Messages = $this->Paginator->paginate('Message');

		$this->set('Messages', $Messages);		

		$this->set('Priorities',$this->Priorities);

		$keyword = '';
		$group_id = '';
		$filterConditions = array();
		$groupFilter = array();
		$keywordFilter = array();

		if($this->request->is('post')){
			$keyword = $this->request->data['search']['keyword'];
			$group_id = $this->request->data['search']['group_id'];
			if(!empty($keyword)){
				$keywordFilter = array('AND' => array(
												array(
													 "Message.comment LIKE '%".$keyword."%'",
												),
												array(
													 "Message.author_name LIKE '%".$keyword."%'",
												),
											),
								);	
			}

			if(!empty($group_id)){
				$groupFilter = array(
					'AND' =>
						array(
							 "Message.group_id" => $group_id,
						),
				);
			}			

			$filterConditions = array($keywordFilter, $groupFilter);
		}

		if(!AuthComponent::user('app_user')){
			$this->set('page_title','Messages');
			$this->render('/Messages/index');
		}
		else{			
			$this->set('page_title','Dashboard');
			$totalMessages = $this->Message->find('count', [
				'conditions' => [
					'Message.active' => 1,
				],
			]);
			$totalOpenMessages = $this->Message->find('count', [
				'conditions' => [
					'Message.active' => 1,
					'Message.closed' => 0,
				],
			]);
			$totalClosedMessages = $this->Message->find('count', [
				'conditions' => [
					'Message.active' => 1,
					'Message.closed' => 1,
				],
			]);
			$this->set('totalMessages', $totalMessages);
			$this->set('totalOpenMessages', $totalOpenMessages);
			$this->set('totalClosedMessages', $totalClosedMessages);

			if(AuthComponent::user('teamlead')){
				$myTotalMessages = $this->Message->find('count', [
					'conditions' => [
						'Message.active' => 1,
						'Group.team_lead_id'=> AuthComponent::user('id'),
					],
				]);
				$myTotalOpenMessages = $this->Message->find('count', [
					'conditions' => [
						'Message.active' => 1,
						'Group.team_lead_id'=> AuthComponent::user('id'),
						'Message.closed' => 0,
					],
				]);
				$myTotalClosedMessages = $this->Message->find('count', [
					'conditions' => [
						'Message.active' => 1,
						'Group.team_lead_id'=> AuthComponent::user('id'),
						'Message.closed' => 1,
					],
				]);
				$myTotalGroups = $this->Group->find('count', [
					'conditions' => [
						'Group.active' => 1,
						'Group.team_lead_id'=> AuthComponent::user('id'),
					],
				]);
				$this->set('myTotalMessages', $myTotalMessages);
				$this->set('myTotalOpenMessages', $myTotalOpenMessages);
				$this->set('myTotalClosedMessages', $myTotalClosedMessages);
				$this->set('myTotalGroups', $myTotalGroups);				

				$this->Paginator->settings = array(
					'Message' => array(
			        	'conditions' => ['Message.active' => 1, 'Group.team_lead_id' => AuthComponent::user('id'), 'Message.closed' => 0, $filterConditions],
			        	'limit' => 10,
				        'order' => array(
				        	'Message.priority' => 'desc',
				            'Message.id' => 'desc',				            
				        )
			        ),
				);				

				$Messages = $this->Paginator->paginate('Message');
				$this->set('Messages', $Messages);
				$this->render('/Dashboards/index_teamlead');
			}
			elseif(!AuthComponent::user('admin')){
				$this->Paginator->settings = array(
					'Message' => array(
			        	'conditions' => ['Message.active' => 1, 'Message.assigned_to' => AuthComponent::user('id'), 'Message.closed' => 0, $filterConditions],
			        	'limit' => 10,
				        'order' => array(
				        	'Message.priority' => 'desc',
				            'Message.id' => 'desc',				            
				        )
			        ),
				);				

				$MyMessages = $this->Paginator->paginate('Message');
				$this->set('MyMessages', $MyMessages);
				$this->render('/Dashboards/index_support');
			}

			if(AuthComponent::user('admin')){
				$this->Paginator->settings = array(
					'Message' => array(
			        	'conditions' => ['Message.active' => 1, 'Message.closed' => 0, $filterConditions],
			        	'limit' => 10,
				        'order' => array(
				        	'Message.priority' => 'desc',
				            'Message.id' => 'desc',				            
				        )
			        ),
				);	
				$Messages = $this->Paginator->paginate('Message');
				$this->set('Messages', $Messages);
				$this->set('page_title','Dashboard');
				//$this->render('/Dashboards/index');
				
			}
		}
	}
}
