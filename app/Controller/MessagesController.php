<?php

/**
* Dashboard Controller
*/

App::uses('AppController', 'Controller');

class MessagesController extends AppController
{
    public $paginate = array(
    	'conditions' => ['Message.active' => 1],
    	'limit' => 25,
        'order' => array(
            'Message.id' => 'desc'
        ),
    );
	
	public function index()
	{
		$this->set('title','Messages :: Ticket App');
		$this->set('page_title','Messages');

		$Groups = $this->Group->find('list', [
					'conditions' => ['Group.active' => 1],
					'order' => ['Group.id DESC'],
					'fields'=> ['id','name'],
				]);

		$this->set('Groups', $Groups);

		$keyword = '';
		$group_id = '';
		$filterConditions = array();
		$groupFilter = array();
		$keywordFilter = array();

		if($this->request->is('post')){
			$keyword = $this->request->data['search']['keyword'];
			$group_id = $this->request->data['search']['group_id'];
			if(!empty($keyword)){
				$keywordFilter = array('AND' => array(
												array(
													 "Message.comment LIKE '%".$keyword."%'",
												),
												array(
													 "Message.author_name LIKE '%".$keyword."%'",
												),
											),
								);	
			}

			if(!empty($group_id)){
				$groupFilter = array(
					'AND' =>
						array(
							 "Message.group_id" => $group_id,
						),
				);
			}			

			$filterConditions = array($keywordFilter, $groupFilter);
		}

		$this->Paginator->settings = $this->paginate;

		$this->Paginator->settings = array(
					'Message' => array(
			        	'conditions' => ['Message.active' => 1, $filterConditions],
			        	'limit' => 10,
				        'order' => array(
				        	'Message.priority' => 'desc',
				            'Message.id' => 'desc',				            
				        )
			        ),
				);

		$Messages = $this->Paginator->paginate('Message');

		$this->set('Messages', $Messages);

		$this->set('Priorities',$this->Priorities);

		if(!AuthComponent::user('app_user')){
			$this->set('page_title','Messages');
			//$this->render('index_customer');
		}		
	}

	public function add()
	{
		if ($this->request->is('post')) {
			
			$this->request->data['Message']['user_id'] = AuthComponent::user('id');
			$this->request->data['Message']['author_name'] = AuthComponent::user('first_name').' '. AuthComponent::user('last_name');

            $this->request->data['Message']['active'] = 1;
            $this->request->data['Message']['status'] = 'New Message';

            $this->Message->create();

            if ($this->Message->save($this->request->data)) {

                $this->Flash->set('Your message have been sent', array(
                	'params' => array(
                		'class' => 'alert alert-success',
                	),
                ));

                $this->redirect(array('action' => 'index'));

            } else {
                $this->Flash->set('Your message could not be send. Please try again', array(
                	'params' => array(
                		'class' => 'alert alert-danger',
                	),
                ));
            }   
        }
        else{
        	$this->redirect(array('action' => 'index'));
        }
	}

	public function view($id=null)
	{
		$this->set('title','View Message :: Ticket App');

		$Message = $this->Message->findById($id);

		$Group = $this->Group->findById($Message['Message']['group_id']);

		$ClosedBy = $this->User->findById($Message['MessageAction']['user_id']);
		
		$SuportUsers = Hash::combine($Group['Members'], '{n}.id', '{n}.username');

		$this->set('Ranks', $this->Ranks);

		if ($this->request->is('post')) {		
			if(!empty($this->request->data['AssignUser']['assigned_to'])){
				$Message['Message']['status'] = 'Assigned';
				$Message['Message']['assigned_to'] = $this->request->data['AssignUser']['assigned_to'];
				$this->Message->save($Message);
				$messageActionModel = [
					'user_id' => $this->request->data['AssignUser']['assigned_to'], 
					'message_id' => $id,
					'created_by' => AuthComponent::user('id'),
					'updated_by' => AuthComponent::user('id'),
				];
				
				if(!$this->MessageAction->hasAny(['MessageAction.message_id' => $id])){
					$this->MessageAction->create();
					$this->MessageAction->save($messageActionModel);
				}
				else{
					$messageActionModel= $this->MessageAction->find('first', [
						'conditions' => [
							'MessageAction.active' => 1,
							'MessageAction.message_id' => $id,
						]
					]);
					$messageActionModel['MessageAction']['user_id'] = $this->request->data['AssignUser']['assigned_to'];
					$messageActionModel['MessageAction']['updated_by'] = AuthComponent::user('id');
					$this->MessageAction->save($messageActionModel['MessageAction']);
				}

				$this->Flash->set('User has been assigned to this ticket', array(
                	'params' => array(
                		'class' => 'alert alert-success',
                	),
                ));
			}
			if(!empty($this->request->data['MessageAction']['comment'])){
				$Message['Message']['closed'] = $this->request->data['MessageAction']['closed'];
				$Message['Message']['status'] = 'Closed';
				$this->Message->save($Message);

				$messageActionModel= $this->MessageAction->find('first', [
												'conditions' => [
													'MessageAction.active' => 1,
													'MessageAction.message_id' => $id,
													'MessageAction.user_id' => AuthComponent::user('id'),
												]
											]);
				$messageActionModel['MessageAction']['comment'] = $this->request->data['MessageAction']['comment'];
				$messageActionModel['MessageAction']['closed'] = $this->request->data['MessageAction']['closed'];
				$messageActionModel['MessageAction']['customer_rank'] = $this->request->data['MessageAction']['customer_rank'];				

				$messageActionModel['MessageAction']['updated_by'] = AuthComponent::user('id');
				$this->MessageAction->save($messageActionModel['MessageAction']);
			}
				//var_dump($this->request->data);		
			$Message = $this->Message->findById($id);
		}

		$this->set('SuportUsers',$SuportUsers);
		$this->set('ClosedBy',$ClosedBy);
		$this->set('Message',$Message);
	}
}
