<?php

/**
* Dashboard Controller
*/

App::uses('AppController', 'Controller');

class SeedersController extends AppController
{
	var $uses = array('User', 'Group', 'Message');
	
	public function index()
	{
		// create user
		/**/
		
		require_once '../../vendors/fzaninotto/faker/src/autoload.php';

		$faker = \Faker\Factory::create();

		/*
		for($i=5;$i<100;$i++){
			$User = array();
			$User['User']['first_name'] = $faker->firstName;
			$User['User']['last_name'] = $faker->lastName;
			$User['User']['username'] = $faker->safeEmail;
			$User['User']['password'] = '123456';

			$this->request->data = $User;
			$this->request->data['User']['user_group_id'] = 1;
            $this->request->data['User']['status'] = 'New User';

            $this->User->create();
            $this->User->save($this->request->data);

		}
		 */
		for($i=1;$i<100;$i++){
			
			$Message = array();

			$userID = mt_rand(3,99);
			$User = $this->User->findById($userID);
			$User = $User['User'];

			$group_id = mt_rand(1,4);

			$Message['Message']['comment'] = $faker->text(500);
			$Message['Message']['author_name'] = $User['first_name'] . ' '. $User['last_name'];
			$Message['Message']['priority'] ='Normal';
			$Message['Message']['user_id'] = $User['id'];
			$Message['Message']['group_id'] = $group_id;
			$Message['Message']['status'] = 'New Message';

			$this->request->data = $Message;

            $this->Message->create();

            $this->Message->save($this->request->data);

		}	
		
	}
}