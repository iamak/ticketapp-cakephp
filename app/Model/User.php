<?php

App::uses('AppModel', 'Model');

App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	
	public $profilePhotoUploads = 'uploads/users';

    public $belongsTo = array(
        'UserGroup' => array(
            'className' => 'UserGroup',
            'foreignKey' => 'user_group_id',
            'conditions' => array('UserGroup.active' => 1),
            'fields' => 'name'
        )
    );

    public $hasAndBelongsToMany = array(
        'MemberOf' => array(
            'className' => 'Group',
        )
    );


    public $validate = array(
        'first_name' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid first name',
            ),
        ),
        'last_name' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid last name',
            ),
        ),
        'username' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid email address',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'This username has already been taken.'
            ),
            'email' => array(
                'rule' => 'email',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Please enter a valid email address'
            ),
        ),
        'password' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid password',
            ),
            'lengthBetween' => array(
                'rule'    => array('lengthBetween', 5, 25),
                'message' => 'Passwords must be between 5 and 25 characters long.'
            )
        ),
        'password_confirm' => array(            
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid password',
            ),
            'lengthBetween' => array(
                'rule'    => array('lengthBetween', 5, 25),
                'message' => 'Passwords must be between 5 and 25 characters long.'
            ),
            'compare'    => array(
                'rule'      => array('validate_passwords'),
                'message' => 'The passwords you entered do not match.',
            )
        ),
        'password_update_confirm' => array(
            'compare'    => array(
                'rule'      => array('validate_update_passwords'),
                'message' => 'The passwords you entered do not match.',
            )
        ),
    );

    /**
     * Custom validation rules
     */
    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['password_confirm'];
    }
    public function validate_update_passwords() {
        return $this->data[$this->alias]['password_update'] === $this->data[$this->alias]['password_update_confirm'];
    }

	/**
     * Before Save
     * @param array $options
     * @return boolean
     */
     public function beforeSave($options = array()) {
        $passwordHasher = new BlowfishPasswordHasher();
        // if ID is not set, we're inserting a new user as opposed to updating
        if (!$this->id) {            
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        else{
            if(!empty($this->data[$this->alias]['password_update'])){
                $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password_update']);
            }
        }        
        // fallback to our parent
        return parent::beforeSave($options);
    }
    
}
