<?php

App::uses('AppModel', 'Model');

class Message extends AppModel {

    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => array('Group.active' => 1),
        ),
        'AssignedUser' => array(
            'className' => 'User',
            'foreignKey' => 'assigned_to',
            'conditions' => array('AssignedUser.active' => 1),
        ),
    );

    public $hasOne = array(
        'MessageAction' => array(
            'className' => 'MessageAction',
            'foreignKey' => 'message_id',
            'conditions' => array('MessageAction.active' => 1),
        ),
    );

    public $validate = array(
        'comment' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter your message',
            ),
        ),
        'author_name' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid author name',
            ),
        ),
        'user_id' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Your user id could not be identified',
            ),
        ),
        'group_id' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please select the group',
            ),
        ),
        'priority' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please select the priority',
            ),
        ),
    );
}
