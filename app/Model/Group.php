<?php

App::uses('AppModel', 'Model');

class Group extends AppModel {	

    public $validate = array(        
        'name' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid email address',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'This group name has already been taken.',
            ),
        ),
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'team_lead_id',
            'conditions' => array('User.active' => 1),
            'fields' => 'User.username'
        )
    );

  
    public $hasAndBelongsToMany = array(
        'Members' => array(
            'className' => 'User',
            'conditions' => array('Members.active' => 1),
        )
    );
    
  	/**
     * Before Save
     * @param array $options
     * @return boolean
     */
     public function beforeSave($options = array()) {      
        // fallback to our parent
        return parent::beforeSave($options);
    }
    
}
