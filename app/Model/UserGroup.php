<?php

App::uses('AppModel', 'Model');

class UserGroup extends AppModel {	

    public $validate = array(        
        'name' => array(
            'ruleRequired' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid email address',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'This group name has already been taken.',
            ),
        ),
    );
    
  	/**
     * Before Save
     * @param array $options
     * @return boolean
     */
     public function beforeSave($options = array()) {      
        // fallback to our parent
        return parent::beforeSave($options);
    }
    
}
