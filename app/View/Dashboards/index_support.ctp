<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $page_title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Statistics</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-comments"></i>
                                </div>
                                <div class="count">
                                    <?=($totalMessages);?>
                                </div>
                                <h3>Total Messages</h3>
                                <p>All Tickets</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-comments-o"></i>
                                </div>
                                <div class="count">
                                    <?=($totalClosedMessages);?>
                                </div>
                                <h3>Resolved Messages</h3>
                                <p>Closed Tickets</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-envelope"></i>
                                </div>
                                <div class="count">
                                    <?=($totalOpenMessages);?>
                                </div>
                                <h3>UnResolved</h3>
                                <p>Open Tickets</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-th"></i>
                                </div>
                                <div class="count">
                                    <?=sizeof($Groups);?>
                                </div>
                                <h3>Total Groups</h3>
                                <p>All Groups</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>My Tickets</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-lg-12 col-md-12 pull-right">
                        <!-- start project list -->
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th width="1%">#</th>
                                    <th width="20%">Author Name</th>
                                    <th>Message</th>
                                    <th width="10%">Group</th>
                                    <th width="10%">Assigned To</th>
                                    <?php
                                            if(AuthComponent::user('app_user')){
                                                ?>
                                        <th width="10%">Priority</th>
                                        <?php
                                            }
                                            ?>
                                            <th width="5%">Likes</th>
                                            <th width="10%"><span class="fa fa-gear"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //debug($Messages);
                                foreach ($MyMessages as $key => $value) {
                                    $Group = $value['Group'];
                                    $Message = $value['Message'];
                                    ?>
                                    <tr>
                                        <td>#
                                            <?= $Message['id'];?>
                                        </td>
                                        <td>
                                            <a>
                                                <?= $Message['author_name'];?>
                                            </a>
                                            <br />
                                            <small>Created <?= date('d-m-Y', strtotime($Message['created_on']));?></small>
                                        </td>
                                        <td>
                                            <p>
                                                <?php
                                                echo substr($Message['comment'], 0, 100);
                                                ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?= $Group['name'];?>
                                        </td>
                                        <td>
                                            <?= $value['AssignedUser']['username'];?>
                                        </td>
                                        <?php
                                            if(AuthComponent::user('app_user')){
                                                ?>
                                            <td>
                                                <?php
                                            $class='default';
                                            if($Message['priority'] === 'Normal'){
                                                $class="success";
                                            }
                                            if($Message['priority'] === 'Low'){
                                                $class="info";
                                            }
                                            if($Message['priority'] === 'Urgent'){
                                                $class="danger";
                                            }
                                            ?>
                                                    <span class="label label-<?=$class;?>"><?= $Message['priority'];?></span>
                                            </td>
                                            <?php
                                            }
                                            ?>
                                                <td>
                                                    <?= $Message['likes'];?>
                                                </td>
                                                <td>
                                                    <a href="/messages/view/<?=$Message['id'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                                </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <!-- end project list -->
                        <div class="clearfix">
                            <ul class="pagination pagination-right">
                                <?php                                     
                                    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
