<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $page_title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    // for admin/support users, no need to show create message form
    if(!AuthComponent::user('app_user')){
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 pull-left">
                                <h2>Send New Message</h2>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 pull-right">
                                <?php echo $this->Form->create('Message', ['class' => 'form-horizontal form-label-lef', 'url' => '/messages/add']); ?>
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <?php
                                        echo $this->Form->input('group_id',['class' => 'select2_single form-control', 'label'=> 'Group', 'type'    => 'select', 'options' => $Groups, 'empty' => false, 'required' => 'required']);
                                    ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <?php
                                        echo $this->Form->input('priority',['class' => 'select2_single form-control', 'label'=> 'Priority', 'type'    => 'select', 'options' => $Priorities, 'empty' => false, 'required' => 'required']);
                                    ?>
                                    </div>
                                    <div class="col-lg-12 col=md-12 form-group block">
                                        <label for="MessageComment">Message</label>
                                        <?php
                                        echo $this->Form->textarea('comment',['class' => 'textarea form-control', 'label'=> 'Message', 'required' => 'required', 'placeholder' => 'Enter your comment']);
                                    ?>
                                    </div>
                                    <div class="clr clearfix"></div>
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <?php echo $this->Form->button('Submit Message', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                                    </div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 pull-left">
                                    <h2>Search Messages</h2>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 pull-right">
                                    <?php echo $this->Form->create('search', ['class' => 'form-horizontal form-label-lef', 'url' => '/messages']); ?>
                                    <div class="row clearfix">
                                        <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                            <?php
                                        echo $this->Form->input('keyword',['maxlength' => 50, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Keyword','label'=> 'Search Messages']);
                                        ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 form-group">
                                            <?php
                                        echo $this->Form->input('group_id',['class' => 'select2_single form-control', 'label'=> 'Filter Group', 'type'    => 'select', 'options' => $Groups]);
                                    ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 form-group">
                                            <label>&nbsp;</label>
                                            <br>
                                            <?php echo $this->Form->button('Search Messages', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                                        </div>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 pull-left">
                                    <h2>Messages</h2>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="row clearfix">
                                <ul class="messages">
                                    <?php
                                    // debug($Messages);
                                foreach ($Messages as $key => $Message) {
                                    $Message = $Message['Message'];
                                    ?>
                                        <li>
                                            <div class="message_date">
                                                <h3 class="date text-info"><?= date('d', strtotime($Message['created_on']));?></h3>
                                                <p class="month">
                                                    <?= date('M / y', strtotime($Message['created_on']));?>
                                                </p>
                                            </div>
                                            <div class="message_wrapper">
                                                <h4 class="heading">
                                            <a href="/messages/view/<?= $Message['id'];?>">
                                                <?= $Message['author_name'];?>
                                            </a>
                                        </h4>
                                                <blockquote class="message">
                                                    <?php
                                                echo substr($Message['comment'], 0, 200);
                                                ?>
                                                </blockquote>
                                                <br>
                                                <!-- <p class="url">
                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                        </p> -->
                                            </div>
                                        </li>
                                        <?php
                        }
                        ?>
                                </ul>
                            </div>
                            <div class="clearfix">
                                <ul class="pagination pagination-right pull-right">
                                    <?php                                     
                                    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
