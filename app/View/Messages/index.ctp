<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $page_title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    // for admin/support users, no need to show create message form
    if(!AuthComponent::user('app_user')){
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 pull-left">
                                <h2>Send New Message</h2>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 pull-right">
                                <?php echo $this->Form->create('Message', ['class' => 'form-horizontal form-label-lef', 'url' => '/messages/add']); ?>
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <?php
                                        echo $this->Form->input('group_id',['class' => 'select2_single form-control', 'label'=> 'Group', 'type'    => 'select', 'options' => $Groups, 'empty' => false, 'required' => 'required']);
                                    ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <?php
                                        echo $this->Form->input('priority',['class' => 'select2_single form-control', 'label'=> 'Priority', 'type'    => 'select', 'options' => $Priorities, 'empty' => false, 'required' => 'required']);
                                    ?>
                                    </div>
                                    <div class="col-lg-12 col=md-12 form-group block">
                                        <label for="MessageComment">Message</label>
                                        <?php
                                        echo $this->Form->textarea('comment',['class' => 'textarea form-control', 'label'=> 'Message', 'required' => 'required', 'placeholder' => 'Enter your comment']);
                                    ?>
                                    </div>
                                    <div class="clr clearfix"></div>
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <?php echo $this->Form->button('Submit Message', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                                    </div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 pull-left">
                                    <h2>Search Messages</h2>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 pull-right">
                                    <?php echo $this->Form->create('search', ['class' => 'form-horizontal form-label-lef', 'url' => '/messages']); ?>
                                    <div class="row clearfix">
                                        <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                                            <?php
                                        echo $this->Form->input('keyword',['maxlength' => 50, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Keyword','label'=> 'Search Messages']);
                                        ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 form-group">
                                            <?php
                                        echo $this->Form->input('group_id',['class' => 'select2_single form-control', 'label'=> 'Filter Group', 'type'    => 'select', 'options' => $Groups]);
                                    ?>
                                        </div>
                                        <div class="col-md-4 col-sm-4 form-group">
                                            <label>&nbsp;</label>
                                            <br>
                                            <?php echo $this->Form->button('Search Messages', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                                        </div>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 pull-left">
                                    <h2>Messages</h2>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped projects">
                                <thead>
                                    <tr>
                                        <th width="1%">#</th>
                                        <th width="20%">Author Name</th>
                                        <th>Message</th>
                                        <th width="10%">Group</th>
                                        <th width="10%">Assigned To</th>
                                        <?php
                                            if(AuthComponent::user('app_user')){
                                                ?>
                                            <th width="10%">Priority</th>
                                            <?php
                                            }
                                            ?>
                                                <th width="5%">Status</th>
                                                <th width="5%">Likes</th>
                                                <th width="10%"><span class="fa fa-gear"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                //debug($Messages);
                                foreach ($Messages as $key => $value) {
                                    $Group = $value['Group'];
                                    $Message = $value['Message'];
                                    ?>
                                        <tr>
                                            <td>#
                                                <?= $Message['id'];?>
                                            </td>
                                            <td>
                                                <a>
                                                    <?= $Message['author_name'];?>
                                                </a>
                                                <br />
                                                <small>Created <?= date('d-m-Y', strtotime($Message['created_on']));?></small>
                                            </td>
                                            <td>
                                                <p>
                                                    <?php
                                                echo substr($Message['comment'], 0, 100);
                                                ?>
                                                </p>
                                            </td>
                                            <td>
                                                <?= $Group['name'];?>
                                            </td>
                                            <td>
                                                <?= $value['AssignedUser']['username'];?>
                                            </td>
                                            <?php
                                            if(AuthComponent::user('app_user')){
                                                ?>
                                                <td>
                                                    <?php
                                            $class='default';
                                            if($Message['priority'] === 'Normal'){
                                                $class="success";
                                            }
                                            if($Message['priority'] === 'Low'){
                                                $class="info";
                                            }
                                            if($Message['priority'] === 'Urgent'){
                                                $class="danger";
                                            }
                                            ?>
                                                        <span class="label label-<?=$class;?>"><?= $Message['priority'];?></span>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                                    <td>
                                                        <?php 
                                                        if($Message['closed']){
                                                            echo '<span class="label label-success">Closed</span>';
                                                        }
                                                        else{
                                                            echo '<span class="label label-warning">Open</span>';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?= $Message['likes'];?>
                                                    </td>
                                                    <td>
                                                        <a href="/messages/view/<?=$Message['id'];?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                                    </td>
                                        </tr>
                                        <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <div class="clearfix">
                                <ul class="pagination pagination-right">
                                    <?php                                     
                                    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
