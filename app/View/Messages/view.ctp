<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php
        if(AuthComponent::user('app_user'))
        {            
            if($Message['Group']['team_lead_id'] == AuthComponent::user('id') && $Message['Message']['closed'] == 0 )
            {
                ?>
            <div class="x_panel">
                <div class="x_title">
                    <?php
                        if($Message['AssignedUser']['id']){
                        ?>
                        <h2>Assigned to : <?=$Message['AssignedUser']['username'];?></h2>
                        <?php
                        }
                        else{
                        ?>
                            <h2>Assign to User</h2>
                            <?php
                        }
                        ?>
                                <div class="clr clearfix"></div>
                                <?php echo $this->Form->create('AssignUser', ['class' => 'form-horizontal inline-block form-label-lef']); ?>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <?php
                                    echo $this->Form->input('assigned_to',['class' => 'select2_single form-control', 'label'=> 'Select User', 'type'    => 'select', 'options' => $SuportUsers, 'empty' => 'Select User', 'required' => 'required']);
                                ?>
                                    </div>
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <label class="block">&nbsp;</label>
                                        <?php echo $this->Form->button('Assign User', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                                    </div>
                                    <div class="clearfix clr"></div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                </div>
            </div>
            <?php
            }
            if($Message['AssignedUser']['id'] == AuthComponent::user('id') && $Message['Message']['closed'] == 0){
            ?>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Submit Action</h2>
                        <div class="clr clearfix"></div>
                        <?php echo $this->Form->create('MessageAction', ['class' => 'form-horizontal inline-block form-label-lef']); ?>
                        <div class="row">
                            <div class="col-lg-12 col=md-12 form-group block">
                                <label for="MessageComment">Comment</label>
                                <?php
                                        echo $this->Form->textarea('comment',['class' => 'textarea form-control', 'label'=> 'Comment', 'required' => 'required', 'placeholder' => 'Enter your comment']);
                                    ?>
                            </div>
                            <div class="clr clearfix"></div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <?php
                                    echo $this->Form->input('customer_rank',['class' => 'select2_single form-control', 'label'=> 'Customer Rank', 'type'    => 'select', 'options' => $Ranks, 'empty' => 'Select Rank', 'required' => 'required']);
                                ?>
                            </div>
                            <div class="clr clearfix"></div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <?php
                                    echo $this->Form->input('closed',['type' => 'checkbox', 'class' => 'js-switch', 'value' => 1, 'label' => 'Close Message']);
                                    ?>
                                </div>
                            </div>
                            <div class="clr clearfix"></div>
                            <div class="col-md-4 col-sm-4 form-group">
                                <label class="block">&nbsp;</label>
                                <?php echo $this->Form->button('Submit Action', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                            </div>
                            <div class="clearfix clr"></div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <?php                
            }
        }
            ?>
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="block no-float"><?=$Message['Message']['author_name'];?>
                        <small><?php echo date('dS M y', strtotime($Message['Message']['created_on']));?> 
                        <?php
                        $class='default';
                        if($Message['Message']['priority'] === 'Normal'){
                            $class="success";
                        }
                        if($Message['Message']['priority'] === 'Low'){
                            $class="info";
                        }
                        if($Message['Message']['priority'] === 'Urgent'){
                            $class="danger";
                        }
                        ?>
                        <label class="label label-<?=$class;?>"><?= $Message['Message']['priority'];?></label>
                        <?php
                        if($Message['Message']['closed']){
                            echo '<label class="label label-success">Closed</label>';
                        }
                        else{
                            echo '<label class="label label-warning">Open</label>';
                        }
                        ?>
                    </small>
                    <span class="pull-right"><?php
                    echo ($Message['Group']['name']);
                    ?></span>
                </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="bs-example" data-example-id="simple-jumbotron">
                            <div class="jumbotron">
                                <p>
                                    <?=$Message['Message']['comment'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <?php
                // if user already liked
                ?>
                            <button class="btn btn-info"><span class="fa fa-thumbs-up"></span> Like</button>
                    </div>
                </div>
                <?php
                if($Message['Message']['closed']){
                    ?>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 class="block no-float">Closed By <?=$ClosedBy['User']['username'];?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="bs-example" data-example-id="simple-jumbotron">
                                <div class="jumbotron">
                                    <p>
                                        <?=$Message['MessageAction']['comment'];?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
    </div>
</div>
