<div class="row clearfix">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo @$page_title;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php 
                    echo $this->Form->create('User', ['class' => 'form-horizontal form-label-lef']);
                    echo $this->Form->input('id');
                ?>
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php 
                            echo $this->Form->input('first_name',['maxlength' => 50, 'required' => true, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'First Name']);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('last_name',['maxlength' => 50, 'required' => true, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Last Name']);
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php
                        echo $this->Form->input('username',['maxlength' => 80, 'required' => true, 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email ID']);
                        ?>
                        </div>
                    </div>
                    <div class="clr clearfix"></div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php
                        echo $this->Form->input('password_update',['type' => 'password', 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Password', 'required' => false]);
                        ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php
                        echo $this->Form->input('password_update_confirm',['type' => 'password', 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'required' => false]);
                        ?>
                        </div>
                    </div>
                    <div class="clr clearfix"></div>
                    <?php
                    // below feature only available for support user
                    if($User['user_group_id'] == 2){
                        ?>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <?php
                        echo $this->Form->input('admin',['type' => 'checkbox', 'class' => 'js-switch', 'value' => 1, 'label' => 'Make as Admin User']);
                        ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <?php
                        echo $this->Form->input('app_user',['type' => 'checkbox', 'class' => 'js-switch', 'value' => 1, 'label' => 'Support User']);
                        ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group">
                                <?php
                        echo $this->Form->input('teamlead',['type' => 'checkbox', 'class' => 'js-switch', 'value' => 1, 'label' => 'Team Lead']);
                        ?>
                            </div>
                        </div>
                        <div class="clr clearfix"></div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <?php
                        echo $this->Form->input('MemberOf',['class' => 'select2_multiple form-control','multiple' => 'multiple', 'label'=> 'Select Groups for this user', 'type' => 'select', 'options' => $Groups, 'empty' => 'Please select']);
                    ?>
                            </div>
                        </div>
                        <div class="clr clearfix"></div>
                        <?php
                    }
                    ?>
                            <!-- <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <?php
                        //echo $this->Form->input('teamlead',['type' => 'checkbox', 'class' => 'js-switch', 'value' => 1, 'label' => 'Make as Team Lead']);
                        ?>
                        </div>
                    </div> -->
                </div>
                <div class="ln_solid"></div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <?php echo $this->Form->button('Update User', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /.row -->
<div class="clr"></div>
