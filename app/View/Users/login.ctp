<div class="animate form login_form">
    <section class="login_content">                
        <?php echo $this->Form->create('User'); ?>
        <h1><?php echo $page_title;?></h1>
        
        <?php 

        echo $this->Form->input('username',['required' => true, 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email ID / Username', 'label' => '']);

        echo $this->Form->input('password',['required' => true, 'class' => 'form-control', 'placeholder' => 'Password', 'label' => '']);

        ?>

        <div>
            <?php echo $this->Form->button('Login', ['type'=>'submit', 'class' => 'btn btn-default submit']); ?>
        </div>
        <div class="clearfix"></div>
        <div class="separator">
            <p class="change_link">Don't have account?
                <a href="/users/register" class="to_register"> Create New Account </a>
            </p>
            <div class="clearfix"></div>
            <br />
            <div>
                <h1>Ticket App</h1>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>
</div>
