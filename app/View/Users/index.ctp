<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $page_title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row clearfix">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Users Lists</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">ID #</th>
                                <th width="30%">Full Name</th>
                                <th width="40%">Email ID</th>
                                <th width="15%">Previlages</th>
                                <th width="5%"><span class="fa fa-gear"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //debug($Users);
                        foreach ($Users as $key => $User) {
                            $UserGroup = $User['UserGroup'];
                            $User = $User['User'];
                            ?>
                                <tr>
                                    <th>
                                        <a href="/users/edit/<?=$User['id'];?>">
                                            <?=$User['id'];?>
                                        </a>
                                    </th>
                                    <td>
                                        <a href="/users/edit/<?=$User['id'];?>">
                                            <?=$User['first_name'].' '.$User['last_name'];?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/users/edit/<?=$User['id'];?>">
                                            <?=$User['username'];?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php
                                        if($User['admin']){
                                            echo '<span class="label label-success">Admin</span>';
                                        }
                                        if($User['teamlead']){
                                            echo '<span class="label label-primary">Team Lead</span>';
                                        }
                                        if($User['app_user']){
                                            echo '<span class="label label-info">Support Member</span>';
                                        }
                                        else{
                                            echo '<span class="label label-warning">Customer</span>';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="/users/edit/<?=$User['id'];?>" title="Edit">
                                            <span class="fa fa-edit"></span>
                                        </a>
                                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="delete-<?=$User['id'];?>">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete User - <?=$User['username'];?>?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure? Do you want to delete this user?</p>
                                                        <p class="alert alert-danger">
                                                            Please note this action cannot be rolled back.
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">                                                        
                                                        <?php
                                                        echo $this->Form->create('User', ['class' => 'form-horizontal form-label-lef', 'url' => '/users/delete/'.$User['id']]);
                                                        echo $this->Form->input('id');
                                                        ?>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <?php echo $this->Form->button('Delete', ['type'=>'submit', 'class' => 'btn btn-danger']); ?>
                                                        <?php echo $this->Form->end(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a title="Delete" data-toggle="modal" data-target="#delete-<?=$User['id'];?>"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                                <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <ul class="pagination pagination-right pull-right">
                            <?php                                     
                                    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                                ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
