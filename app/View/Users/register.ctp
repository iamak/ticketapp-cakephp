<div class="animate form login_form">
    <section class="login_content">
        <?php echo $this->Form->create('User'); ?>
        <h1><?php echo $page_title;?></h1>
        <?php 

        echo $this->Form->input('first_name',['maxlength' => 50, 'required' => true, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'First Name']);

        echo $this->Form->input('last_name',['maxlength' => 50, 'required' => true, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Last Name']);

        echo $this->Form->input('username',['maxlength' => 80, 'required' => true, 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Email ID']);

        echo $this->Form->input('password',['maxlength' => 20, 'required' => true, 'class' => 'form-control', 'placeholder' => 'Password']);

        echo $this->Form->input('password_confirm',['type' => 'password', 'maxlength' => 20, 'required' => true, 'class' => 'form-control', 'placeholder' => 'Confirm Password']);

        ?>
        
        <div>
            <?php echo $this->Form->button('Register', ['type'=>'submit', 'class' => 'btn btn-default submit']); ?>
        </div>
        <div class="clearfix"></div>
        <div class="separator">
            <p class="change_link">Already member?
                <a href="/users/login" class="to_register"> Login</a>
            </p>
            <div class="clearfix"></div>
            <br />
            <div>
                <h1>Ticket App</h1>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>
</div>
