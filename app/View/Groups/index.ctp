<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo $page_title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row clearfix">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Group Lists</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">ID #</th>
                                <th>Name</th>
                                <th width="20%">Team Lead</th>
                                <th width="5%"><span class="fa fa-gear"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //debug($Groups);
                        foreach ($Groups as $key => $Group) {
                            $User = $Group['User'];
                            $Group = $Group['Group'];                            
                            ?>
                                <tr>
                                    <th>
                                        <a href="/groups/edit/<?=$Group['id'];?>">
                                            <?=$Group['id'];?>
                                        </a>
                                    </th>
                                    <td>
                                        <a href="/groups/edit/<?=$Group['id'];?>">
                                            <?=$Group['name'];?>
                                        </a>
                                    </td>
                                    <td>
                                        <?=$User['username'];?>
                                    </td>
                                    <td>
                                        <a href="/groups/edit/<?=$Group['id'];?>" title="Edit"><span class="fa fa-edit"></span></a>
                                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="delete-<?=$Group['id'];?>">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete Group - <?=$Group['name'];?>?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure? Do you want to delete this group?</p>
                                                        <p class="alert alert-danger">
                                                            Please note this action cannot be rolled back.
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">                                                        
                                                        <?php
                                                        echo $this->Form->create('Group', ['class' => 'form-horizontal form-label-lef','url'=>'/groups/delete/'.$Group['id']]); 
                                                        echo $this->Form->input('id', ['type'=>'hidden']);
                                                        ?>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <?php echo $this->Form->button('Delete', ['type'=>'submit', 'class' => 'btn btn-danger']); ?>
                                                        <?php echo $this->Form->end(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a title="Delete" data-toggle="modal" data-target="#delete-<?=$Group['id'];?>"><span class="fa fa-trash"></span></a>
                                    </td>                                    
                                </tr>
                                <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="clearfix">
                        <ul class="pagination pagination-right pull-right">
                            <?php                                     
                                    echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                                ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
