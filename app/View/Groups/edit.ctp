<div class="row clearfix">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo @$page_title;?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
             <?php 
             echo $this->Form->create('Group', ['class' => 'form-horizontal form-label-lef']); 
             echo $this->Form->input('id');
             ?>
                <div class="form-group">
                    <label for="UserGroupName" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                    echo $this->Form->input('name',['maxlength' => 50, 'required' => true, 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Name','label'=> false]);
                    ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Team Lead</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        echo $this->Form->input('team_lead_id',['class' => 'select2_single form-control', 'label'=> false, 'type' => 'select', 'options' => $Users, 'empty' => 'Please Select']);
                    ?>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <?php echo $this->Form->button('Update Group', ['type'=>'submit', 'class' => 'btn btn-success']); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<div class="clr"></div>
