-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2016 at 02:20 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticketapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `team_lead_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `team_lead_id`, `active`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Support', 2, 1, '2016-11-11 18:18:44', '2016-11-12 21:08:43', 1, 1),
(2, 'Sales', 2, 1, '2016-11-11 18:19:03', '2016-11-12 21:20:25', 1, 1),
(3, 'Billing', 3, 1, '2016-11-11 18:22:39', '2016-11-13 05:08:45', 1, 1),
(4, 'Service', 3, 1, '2016-11-11 18:34:11', '2016-11-13 05:08:50', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups_users`
--

CREATE TABLE `groups_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups_users`
--

INSERT INTO `groups_users` (`user_id`, `group_id`) VALUES
(5, 4),
(5, 1),
(4, 3),
(4, 2),
(4, 4),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `likes` bigint(20) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `assigned_to` int(10) UNSIGNED NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `comment`, `author_name`, `priority`, `likes`, `user_id`, `group_id`, `assigned_to`, `created_on`, `updated_on`, `status`, `active`, `closed`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Admi User', 'Normal', 0, 7, 4, 0, '2016-11-13 11:06:40', '2016-11-13 12:52:26', 'New Message', 1, 0),
(2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Normal', 0, 7, 3, 0, '2016-11-13 11:06:44', '2016-11-13 11:06:44', 'New Message', 1, 0),
(3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Normal', 0, 7, 2, 4, '2016-11-13 11:06:52', '2016-11-13 11:06:52', 'New Message', 1, 0),
(4, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Normal', 0, 7, 1, 5, '2016-11-13 11:06:55', '2016-11-13 12:07:59', 'Closed', 1, 1),
(5, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Urgent', 0, 7, 4, 0, '2016-11-13 11:07:01', '2016-11-13 11:07:01', 'New Message', 1, 0),
(6, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Low', 0, 7, 3, 0, '2016-11-13 11:07:08', '2016-11-13 11:07:08', 'New Message', 1, 0),
(7, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Normal', 0, 7, 4, 0, '2016-11-13 11:07:22', '2016-11-13 11:07:22', 'New Message', 1, 0),
(8, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Low', 0, 7, 3, 0, '2016-11-13 11:07:27', '2016-11-13 11:07:27', 'New Message', 1, 0),
(9, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Urgent', 0, 7, 2, 4, '2016-11-13 11:07:33', '2016-11-13 11:07:33', 'New Message', 1, 0),
(10, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, quibusdam. Iusto rem necessitatibus iste, aut omnis. Dolor laborum quidem accusantium ducimus quasi, qui pariatur, similique eum omnis ad ab. Laborum?', 'Customer User', 'Urgent', 0, 7, 1, 5, '2016-11-13 11:07:40', '2016-11-13 12:07:55', 'Closed', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `message_actions`
--

CREATE TABLE `message_actions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `comment` text NOT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `ceated_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `customer_rank` int(11) NOT NULL,
  `support_rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_actions`
--

INSERT INTO `message_actions` (`id`, `user_id`, `message_id`, `comment`, `closed`, `ceated_by`, `updated_by`, `created_on`, `updated_on`, `active`, `customer_rank`, `support_rank`) VALUES
(1, 5, 10, 'asd as sa', 1, 0, 5, '2016-11-13 11:09:03', '0000-00-00 00:00:00', 1, 4, 0),
(2, 4, 9, '', 0, 0, 0, '2016-11-13 11:10:25', '0000-00-00 00:00:00', 1, 0, 0),
(3, 5, 4, 'aljdhljash fkldjahjdkls', 1, 0, 5, '2016-11-13 11:10:31', '0000-00-00 00:00:00', 1, 5, 0),
(4, 4, 3, '', 0, 0, 0, '2016-11-13 11:10:37', '0000-00-00 00:00:00', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `app_user` tinyint(1) NOT NULL,
  `teamlead` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `username`, `password`, `first_name`, `last_name`, `profile_picture`, `active`, `status`, `admin`, `app_user`, `teamlead`, `created_by`, `updated_by`, `updated_on`, `created_on`) VALUES
(1, 2, 'admin@app.com', '$2a$10$8RBm8Kg2bhweh.5uT.CBMOfc8ZDiHqhPAL3PEyoCvW42LwpUc0Srm', 'Admin', 'User', '', 1, 'New User', 1, 1, 0, 0, 0, '2016-11-13 11:03:35', '2016-11-13 11:01:25'),
(2, 2, 't1@app.com', '$2a$10$feGfveO8lToUACxUJ/b9repg0tFpJH/511.GqReXftrjCf1WkD5Ay', 'Team Lead 1', 'Team Lead 1', '', 1, 'New User', 0, 1, 1, 0, 1, '2016-11-13 11:03:46', '2016-11-13 11:01:54'),
(3, 2, 't2@app.com', '$2a$10$5sE7Rhu9u5fox.Bn6BOu/.AROOdGjFGIQyI6pNG7fPaQbzk8.ARPm', 'Team Lead 2', 'Team Lead 2', '', 1, 'New User', 0, 1, 1, 0, 1, '2016-11-13 11:04:01', '2016-11-13 11:02:15'),
(4, 2, 'sp1@app.com', '$2a$10$0G9B8B6Ujv19lLsFBEFupeXWo02mxFuBZORNNFnSIfHfg32yUp9Ra', 'Support 1', 'Support 1', '', 1, 'New App User', 0, 1, 0, 1, 1, '2016-11-13 11:04:35', '2016-11-13 11:04:35'),
(5, 2, 'sp2@app.com', '$2a$10$TTI0AMxhiaDTrOC98lRg4Os2gD3UT9nVVH8YD2oKdPLPksLsB88lm', 'Support 2', 'Support 2', '', 1, 'New App User', 0, 1, 0, 1, 1, '2016-11-13 11:05:11', '2016-11-13 11:04:57'),
(6, 1, 'cs1@app.com', '$2a$10$yZROCQIpUj412UtLjYZpT.QtPmahRhEobMvVsrqhZ571ro9s/R3Ju', 'Customer', 'User', '', 1, 'New User', 0, 0, 0, 0, 0, '2016-11-13 11:05:49', '2016-11-13 11:05:49'),
(7, 1, 'cs2@app.com', '$2a$10$kjuv3v3EEGenR8npqUQTj.Tk3DAA8QJH.IH8PgsvwB1XX2jEzwauW', 'Customer', 'User', '', 1, 'New User', 0, 0, 0, 0, 0, '2016-11-13 11:06:07', '2016-11-13 11:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `active`, `admin_access`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Customer', 1, 0, '2016-11-11 16:03:30', '2016-11-11 16:03:30', 0, 0),
(2, 'Support Team', 1, 1, '2016-11-11 16:03:30', '2016-11-11 16:04:19', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_actions`
--
ALTER TABLE `message_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_group_id` (`user_group_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`,`user_group_id`,`username`,`first_name`,`profile_picture`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `message_actions`
--
ALTER TABLE `message_actions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
